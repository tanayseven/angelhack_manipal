from django import template

register = template.Library()


@register.inclusion_tag('templatetags/field.html')
def render_field(field):
    return {
        'field': field,
    }
