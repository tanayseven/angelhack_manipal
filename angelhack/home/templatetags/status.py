from django import template
from issues.models import STATUS_ISSUE

register = template.Library()


@register.filter
def status_issue(status_key):
    return dict(STATUS_ISSUE).get(status_key)
