from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^sign-up$', views.signup, name='signup'),
    url(r'^sign-in$', views.signin, name='signin'),
    url(r'^sign-out$', views.signout, name='signout'),
    url(r'^daily-routine$', views.dailyroutine, name='routine'),
]
