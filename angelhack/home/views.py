from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.core.urlresolvers import reverse
from .forms import *
import time
import datetime
from django.views.decorators.csrf import csrf_exempt
from issues.models import *
from django.template.loader import render_to_string
import requests

ua = 'Mozilla/5.0 (Linux; Android 6.0.1; MotoG3 Build/MPI24.107-55; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/49.0.2623.105 Mobile Safari/537.36'

def index(request):
    if request.user.is_authenticated():
        return redirect(reverse('issues:all'))
    return render(request, 'home/index.html')

def signup(request):
    if request.method == 'POST':
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            if User.objects.filter(email=form.cleaned_data['email']).count() == 0:
                user = form.save()
                user.set_password(form.cleaned_data['password'])
                user.username = form.cleaned_data['first_name'] + form.cleaned_data['last_name'] + str(time.time()).split('.')[0]
                user.save()
                messages.success(request, "Your account has been created, you can now log in")
                return redirect(reverse('home:signin'))
            else:
                messages.error(request, "This email address is already taken")
        else:
            messages.warning(request, "Some data received are invalid")
    else:
        form = UserSignUpForm()
    return render(request, 'home/signup.html', {'form': form})

@csrf_exempt
def signin(request):
    # print(request.META.get("HTTP_USER_AGENT"))
    if request.method == 'POST':
        form = UserSignInForm(request.POST)
        if form.is_valid():
            try:
                username = User.objects.get(email=form.cleaned_data['email']).username
                user = auth.authenticate(username=username, password=form.cleaned_data['password'])
                if user is not None and user.is_active:
                    auth.login(request, user)
                    messages.success(request, "Your are now connected!")
                    return redirect(reverse('home:index'))
                else:
                    messages.warning(request, "No account found with these credentials")
            except (KeyError, User.DoesNotExist):
                    messages.warning(request, "No account found with these credentials")
        else:
            messages.warning(request, "Some data received are invalid")
    else:
        form = UserSignInForm()
    return render(request, 'home/signin.html', {'form': form})

def signout(request):
    auth.logout(request)
    messages.info(request, "You are now disconnected")
    return redirect(reverse('home:signin'))

def send_mail(to, subject, content):
    requests.post(
        'https://api.mailgun.net/v3/sandboxaa42a819db5c4a8f8ab17535ceffe0ab.mailgun.org/messages',
        auth=("api", 'key-942d65f04a3923ebbd2f27573e797485'),
        data={"from": 'Team Find Figam' + " <report@findfigam.com>",
              "to": to,
              "subject": subject,
              "html": content
          })

def dailyroutine(request):
    categories = Category.objects.all()
    for category in categories:
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        issues = Issue.objects.filter(date_created__gt=yesterday, category=category)
        tpl = render_to_string('home/dailyroutine.html', {'issues':issues, 'category': category, 'date_today': datetime.date.today()})
        send_mail([category.respected.email], "Report of the day", tpl)
    return HttpResponse("done")
