from django.contrib import admin

from .models import *

class ImageInline(admin.StackedInline):
    model = Image
    extra = 1

class IssueAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'status', 'date_created']
    fieldsets = [
        (None, {'fields': ['title', 'summary', 'description', 'category', 'tags']}),
        ('Administration', {'fields': ['status', 'author']}),
        ('Location', {'fields': ['latitude', 'longitude']}),
        ('Date information', {'fields': ['date_created', 'date_updated'], 'classes': ['collapse']})
    ]
    inlines = [ImageInline]

class ImageAdmin(admin.ModelAdmin):
    list_display = ['file', 'title', 'issue']

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title', 'respected_email']
    def respected_email(self, obj):
        return obj.respected.email

admin.site.register(Issue, IssueAdmin)
admin.site.register(Tag)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Image, ImageAdmin)
