from django import forms
from .models import *

class SubmitIssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = ('title', 'summary', 'description', 'tags', 'latitude', 'longitude', 'category')

    def __init__(self, *args, **kwargs):
        super(SubmitIssueForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['summary'].widget.attrs.update({'class': 'form-control', 'rows': '2'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['tags'].widget.attrs.update({'class': 'form-control select2tags'})
        self.fields['latitude'].widget.attrs.update({'class': 'form-control'})
        self.fields['longitude'].widget.attrs.update({'class': 'form-control'})
        self.fields['category'].widget.attrs.update({'class': 'form-control select2select'})

class AddImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'file', 'summary']

    def __init__(self, *args, **kwargs):
        super(AddImageForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['summary'].widget.attrs.update({'class': 'form-control', 'rows': '2'})
