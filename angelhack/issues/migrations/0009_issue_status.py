# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-23 19:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0008_auto_20160423_1756'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='status',
            field=models.CharField(choices=[('waiting_picture', 'Waiting for first picture'), ('online', 'Online'), ('resolved', 'Resolved'), ('duplicate', 'Duplicate'), ('closed', 'Closed')], default='waiting_picture', max_length=40),
        ),
    ]
