from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

STATUS_ISSUE = (
    ('waiting_picture', "Waiting for first picture"),
    ('online', "Online"),
    ('resolved', "Resolved"),
    ('duplicate', "Duplicate"),
    ('closed', "Closed"),
)

class Category(models.Model):
    title = models.CharField(max_length=70)
    respected = models.ForeignKey(User, default=1)
    
    def __str__(self):
        return self.title

class Tag(models.Model):
    title = models.CharField(max_length=70)
    
    def __str__(self):
        return self.title

class Issue(models.Model):
    title = models.CharField(max_length=70)
    summary = models.TextField(max_length=700)
    description = models.TextField(max_length=5000)
    author = models.ForeignKey(User, null=True)
    status = models.CharField(max_length=40, choices=STATUS_ISSUE, default='waiting_picture')

    category = models.ForeignKey(Category, blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True)

    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    
    date_created = models.DateTimeField(default=timezone.now)
    date_updated = models.DateTimeField(null=True)
    
    def __str__(self):
        return self.title

class Image(models.Model):
    file = models.ImageField(upload_to="issue-img/")
    title = models.CharField(max_length=70)
    summary = models.TextField(max_length=700, null=True, blank=True)
    issue = models.ForeignKey(Issue)
    
    def __str__(self):
        return self.title
