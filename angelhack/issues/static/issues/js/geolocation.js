var infoWindow ;
var map;
var mainLat = 0, mainLng = 0, radiusLimit = 0.01;
var strPointed = "Issue Location";
var input = null, searchBox = null;
function initMap() {
    map = new google.maps.Map($('#map')[0], {
	center: {lat: 0.0, lng: 0.0},
	scrollwheel: true,
	zoom: 1,
	mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    infoWindow = new google.maps.InfoWindow({map: map});
    infoWindow.setContent(strPointed);
    google.maps.event.addListener(map, 'click', function(event) {
	var pos = {lat: event.latLng.lat(), lng: event.latLng.lng()};
	updateFormPosition(pos.lat, pos.lng);
	infoWindow.setPosition(pos);
    });
}
function handleLocationError(support) {
    if (support) {
	initMap();
    }
}
function updateFormPosition(lat, lng) {
    $('#id_latitude').val(lat);
    $('#id_longitude').val(lng);
}
$(function(){    
    if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
	    var pos = {
		lat: position.coords.latitude,
		lng: position.coords.longitude
	    };
	    infoWindow.setPosition(pos);
	    infoWindow.setContent(strPointed);
	    map.setCenter(pos);
	    map.setZoom(14);
	    updateFormPosition(pos.lat, pos.lng);
	    mainLat = pos.lat; mainLng = pos.lng;
	}, function() {
	    console.log("Insecure origins errors");
	    handleLocationError(true);
	});
    } else {
	handleLocationError(false);
    }
    $("body").append($("<script />", {
	src: 'https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAqkdOCFGooUp--9EyjYr0DC-fiiBa3neg&callback=initMap'
    }));
});
