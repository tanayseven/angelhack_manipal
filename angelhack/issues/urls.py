from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^test_geolocation/$', views.location, name='geolocation'),
    url(r'^submit$', views.submit, name='submit'),
    url(r'^me$', views.me, name='me'),
    url(r'^all$', views.all, name='all'),
    url(r'^(?P<issue_id>[0-9]+)/images$', views.images, name='images'),
    url(r'^(?P<issue_id>[0-9]+)$', views.details, name='details'),
]
