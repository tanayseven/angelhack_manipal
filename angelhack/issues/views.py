from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils import timezone
from .forms import *
from .models import *

def location(request):
    if request.POST:
        print(request.POST['lat'], request.POST['lng'])
        print("Got coordinates: ", request.POST['lat'], request.POST['lng'])
        return HttpResponse("You just sent me the coordinates: %s %s" % (str(request.POST['lat']), str(request.POST['lng'])))
    else:
        print("Empty call")
        return render(request, 'issues/index.html')

def submit(request):
    if not request.user.is_authenticated():
        messages.error(request, "You must be logged in to submit an issue")
        return redirect(reverse('home:signin'))
    if request.method == 'POST':
        form = SubmitIssueForm(request.POST)
        if form.is_valid():
            form.save()
            issue = Issue.objects.all().order_by('-id').first()
            issue.author = request.user
            issue.save()
            form = SubmitIssueForm()
            messages.success(request, "The issue has been submitted")
            messages.info(request, "You can now add images to your issue")
            return redirect(reverse('issues:images', kwargs={'issue_id': issue.id}))
        else:
            messages.warning(request, "The data received are invalid")
    else:
        form = SubmitIssueForm()
    return render(request, "issues/submit.html", {'form': form})

def images(request, issue_id):
    if not request.user.is_authenticated():
        messages.error(request, "You must be logged in to send an image")
        return redirect(reverse('home:signin'))
    issue = Issue.objects.get(id=issue_id)
    if request.method == 'POST':
        form = AddImageForm(request.POST, request.FILES)
        if form.is_valid():
            newfile = Image(file=request.FILES['file'])
            newfile.title = form.cleaned_data['title']
            newfile.summary = form.cleaned_data['summary']
            newfile.issue = issue
            newfile.save()
            form = AddImageForm()
            issue = Issue.objects.get(id=issue_id)
            if issue.image_set.all().count() > 0:
                if issue.status == 'waiting_picture':
                    issue.status = 'online'
                    issue.save()
            messages.success(request, "The image has been uploaded")
        else:
            messages.warning(request, "The data received are invalid")
    else:
        form = AddImageForm()
    issue = Issue.objects.get(id=issue_id)
    return render(request, "issues/images.html", {"issue": issue, "form": form})

def details(request, issue_id):
    issue = Issue.objects.get(id=issue_id)
    return render(request, "issues/details.html", {"issue": issue})
    

def me(request):
    if not request.user.is_authenticated():
        messages.error(request, "You must be logged in to access this page")
        return redirect(reverse('home:signin'))
    return render(request, "issues/me.html", {"issues": Issue.objects.filter(author=request.user).order_by("-date_created")})

def all(request):
    return render(request, "issues/all.html", {"issues": Issue.objects.filter(status='online').order_by("-date_updated")})
